module test_vm();
   reg clock;
   reg reset;   
   wire [7:0] memio [80*25:0];
   vm VM(clock, memio, reset);
   int 	      i;
   int 	      j;   
   initial begin
      clock <= 1'b0;
      reset <= 1'b0;
      #10;
      clock <= 1'b1;
      #10;
      reset <= 1'b1;      
      #10;      
      clock <= 1'b0;
      $display("Resetting CPU");      

      for (i = 0; i < 40; i = i + 1) begin
	 #10;	 
	 clock <= 1'b1;
	 #10;	 
	 clock <= 1'b0;
	 for (j = 0; j < 40; j = j + 1)
	    $write("%c", memio[j]);
	 $display("\n");	 
      end
   end
endmodule
