module vm(clock, memio, reset);
   reg [7:0] register_table [3:0];
   reg [7:0] ip;
   reg [7:0] memory [4095:0];
   wire [7:0] instruction = memory[ip];
   input wire       clock;
   input wire 	    reset;   
   localparam SCREEN_WIDTH = 80;   
   localparam SCREEN_HEIGHT = 25;
   localparam SCREEN_OFFSET = 256;
   localparam TOTAL_SCREEN_BYTES = SCREEN_WIDTH * SCREEN_HEIGHT;   

   // Connect the memory to the IO
   output wire [7:0] memio [TOTAL_SCREEN_BYTES:0];
   genvar     i;
   generate
      for (i = 0; i < TOTAL_SCREEN_BYTES; i = i + 1)
	assign memio[i] = memory[SCREEN_OFFSET + i];      
   endgenerate

   // Set up ROM
   initial
     $readmemh("rom.txt", memory, 0, 15);
   
   always @ (posedge clock) begin
      $display("IP: ", ip);
   end
   
   always @ (posedge clock) begin
      if (!reset) begin
	 int c;	 
	 for (c = 0; c < 4; c = c + 1)
	   register_table[c] <= 0;
      end  
      else
	case (instruction[7:4])
	  // ALU
	  4'b0000:
	    if ((instruction >> 2) & 8'h3)
	      register_table[0] <= register_table[0] + register_table[instruction & 8'h3];
	    else
	      register_table[0] <= register_table[0] - register_table[instruction & 8'h3];
	  // MOV
	  4'b0010:
	    register_table[(instruction >> 2) & 'h3] <= register_table[(instruction >> 0) & 'h3];
	  // Load Immediate
	  4'b0100:
	    register_table[0] <= (instruction >> 0) & 'hf;
	  // Store
	  4'b1100:
	    begin
	       memory[(('hf & instruction) << 8) | register_table[3]] <= register_table[0];
	       register_table[3] <= register_table[3] + 1;
	    end
	  // Load
	  4'b1101:
	    begin
	       register_table[0] <= memory[(('hf & instruction) << 8) | register_table[2]];
	       register_table[2] <= register_table[2] + 1;
	    end
	endcase // case (instruction[7:4])
   end

   always @ (posedge clock)
     if (!reset)
       ip <= 0;
     else
       case (instruction[7:4])
	 4'b1111:
	   ip <= ip;
	 // Unconditional Relative Jump
	 4'b0001: begin
	    ip <= 1 + (instruction[3] ? (ip - instruction[2:0]) : (ip + instruction[2:0]));
	 end
	 // Conditional Relative Jump
	 4'b1110:
	   if (register_table[0] == 0)
	     ip <= 1 + (instruction[3] ? (ip - instruction[2:0]) : (ip + instruction[2:0]));
	   else
	     if (ip == 4096)
	       ip <= 0;
	     else
	       ip <= ip + 1;	 
	 default:
	   // Increment IP
	   if (ip == 4096)
	     ip <= 0;
	   else
	     ip <= ip + 1;
       endcase
endmodule
